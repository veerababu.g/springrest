package com.hcl.mock.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.mock.model.Student;
import com.hcl.mock.service.StudentService;

@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {
	Student student= new Student();
	Map<String,String> response= new HashMap<>();
	
	@Mock
	StudentService studentService;
	@InjectMocks
	StudentController studentController;
	
	@Before
	public void before() {
		student.setName("G Veerababu");
		student.setDep("Computers");
		response.put("StudentId", "1");
		response.put("Status", "success");
		
	}
	@Test
	public void testCreateStudent() {
		Mockito.when(studentService.createStudent(student)).thenReturn(response);
		assertTrue(studentController.createStudent(student).getBody().containsValue("success"));
	}
	@Test
	public void testGetStudentById() {
		Mockito.when(studentService.getStudentById(1L)).thenReturn(student);
		assertEquals("G Veerababu", studentController.getStudentById(1L).getBody().getName());
		
	}
	@Test
	public void testUpdateStudent() {
		Mockito.when(studentService.updateStudent(student)).thenReturn(response);
		assertTrue(studentController.updateStudent(student).getBody().containsValue("success"));
	}
	
	@Test
	public void testDeleteStudentById() {
		Mockito.when(studentService.deleteStudent(1L)).thenReturn(response);
		assertTrue(studentController.deleteStudentById(1L).getBody().containsValue("success"));
	}
}
