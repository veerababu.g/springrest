package com.hcl.mock.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.mock.model.Student;
import com.hcl.mock.repository.StudentRepository;
import com.hcl.mock.service.StudentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceImplTest {
	Student student= new Student();
	Map<String,String> response= new HashMap<>();
	@Mock
	StudentRepository studentRepository;
	@InjectMocks
	StudentServiceImpl studentService;
	@Before
	public void before() {
		student.setId(1L);
		student.setName("G Veerababu");
		student.setDep("Computers");
		response.put("StudentId", "1");
		response.put("Status", "success");
		
	}
	@Test
	public void testGetStudentById() {
		Mockito.when(studentRepository.getById(1L)).thenReturn(student);
		assertEquals("G Veerababu", studentService.getStudentById(1L).getName());
	}
	@Test
	public void testCreateStudent() {
		Mockito.when(studentRepository.save(student)).thenReturn(student);
		assertEquals("success",studentService.createStudent(student).get("Status"));
		
	}
	/*@Test
	public void testUpdateStudent() {
		Mockito.when(studentRepository.saveAndFlush(student)).thenReturn(student);
		assertEquals("G Veerababu", studentService.getStudentById(1L).getName());
	}*/
	/*@Test
	public void testDeleteStudent() {
		Mockito.doNothing().when(studentService).deleteStudent(1L).clear();
		assertEquals("success",studentService.deleteStudent(1L).get("Status"));
	}*/
	
	

}
