package com.hcl.mock.service;

import java.io.Serializable;
import java.util.Map;

import com.hcl.mock.model.Student;

public interface StudentService extends Serializable {

	public Student getStudentById(Long id);

	public Map<String,String> createStudent(Student student);

	public Map<String, String> updateStudent(Student student);

	public Map<String, String> deleteStudent(Long id);
}
