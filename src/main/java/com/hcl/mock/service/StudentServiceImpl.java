package com.hcl.mock.service;



import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.hcl.mock.exception.StudentNotFoundException;
import com.hcl.mock.model.Student;
import com.hcl.mock.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {

	private static final Logger logger = LogManager.getLogger(StudentServiceImpl.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	public StudentRepository studentRepository;
	
	@Override
	public Student getStudentById(Long id) {
		logger.info("Invoked Student servce to get student name by id...");
		Student student =studentRepository.getById(id);
		if(StringUtils.isEmpty(student))
			throw new StudentNotFoundException("Student id="+ id+" not found");
		return student;
	}

	@Override
	public Map<String, String> createStudent(Student student) {
		Map<String, String> studentResponse= new HashMap<>();
		student=studentRepository.save(student);
		if(!StringUtils.isEmpty(student)) {
			studentResponse.put("StudentId", student.getId().toString());
			studentResponse.put("Status", "success");
		}else {
			studentResponse.put("StudentId", student.getId().toString());
			studentResponse.put("Status", "failed");
		}
			
	
		return studentResponse;
	}

	@Override
	public Map<String, String> updateStudent(Student student) {
		Map<String, String> studentResponse= new HashMap<>();
		student=studentRepository.saveAndFlush(student);
		
		if(!StringUtils.isEmpty(student)) {
			studentResponse.put("StudentId", student.getId().toString());
			studentResponse.put("Status", "success");
		}else {
			studentResponse.put("StudentId", student.getId().toString());
			studentResponse.put("Status", "failed");
		}
		return studentResponse;
	}

	@Override
	public Map<String, String> deleteStudent(Long id) {
		Map<String, String> studentResponse= new HashMap<>();
		studentRepository.deleteById(id);;
		studentResponse.put("StudentId", id.toString());
		studentResponse.put("Status", "success");
		return studentResponse;
	}

}
