package com.hcl.mock.controller;

import java.util.Map;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.mock.model.Student;
import com.hcl.mock.service.StudentService;

@RestController
@RequestMapping(value = "/students")
public class StudentController {
	private static final Logger logger = LogManager.getLogger(StudentController.class);
	@Autowired
	public StudentService studentService;

	@GetMapping("/getStudent/{id}")
	public ResponseEntity<Student> getStudentById(@PathVariable("id") Long id) {
		logger.info("Invoked Student servce controller to get student name by id...");
		return new ResponseEntity<Student>(studentService.getStudentById(id), HttpStatus.OK);

	}
	@PostMapping("/createStudent")
	public ResponseEntity<Map<String, String>> createStudent(@Valid @RequestBody Student student) {
		logger.info("Invoked Student servce controller to insert student Records...");
		return new ResponseEntity<Map<String, String>>(studentService.createStudent(student), HttpStatus.CREATED);
	}
	@PutMapping("/updateStudent")
	public ResponseEntity<Map<String, String>> updateStudent(@RequestBody Student student) {
		logger.info("Invoked Student servce controller to update student Records...");
		return new ResponseEntity<Map<String, String>>(studentService.updateStudent(student), HttpStatus.OK);
	}
	@DeleteMapping("/deleteStudent/{id}")
	public ResponseEntity<Map<String, String>> deleteStudentById(@PathVariable("id") Long id) {
		logger.info("Invoked Student servce controller to delete student Records...");
		return new ResponseEntity<Map<String, String>>(studentService.deleteStudent(id), HttpStatus.OK);

	}
}
