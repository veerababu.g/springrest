package com.hcl.mock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.mock.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	public Student getById(Long studentId);
}
