package com.hcl.mock.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Error
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-07-06T09:52:32.014Z")

@JsonInclude(Include.NON_NULL)
public class Error   {	
	
	@JsonProperty("errorCode")
	private String errorCode = null;

	@JsonProperty("message")
	private String message = null;	
	
	/**
	 * 
	 */
	public Error() {
		super();
	}


	public Error errorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}
  
  
   /**
 * @param code
 * @param message
 * @param description
 */
public Error(String errorCode, String message, String description,String field) {
	super();
	this.errorCode = errorCode;
	this.message = message;	
}




  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public Error message(String message) {
    this.message = message;
    return this;
  } 

  public void setMessage(String message) {
    this.message = message;
  }

  public Error description(String description) {
    //this.description = description;
    return this;
  }

   /**
   * Get fields
   * @return fields
  **/
 /* @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }*/


  

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

