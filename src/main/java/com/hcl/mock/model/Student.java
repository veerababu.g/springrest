package com.hcl.mock.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "student")
public class Student implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "student_id")
	@JsonProperty("StudentId")
	@JsonInclude(Include.NON_NULL)
	private Long id;
	@JsonProperty("StudentName")
	@JsonInclude(Include.NON_NULL)
	@NotBlank(message = "Name should not be empty")
	@Column(name = "student_name")
	@Size(min = 6, message = "Name should have atleast 6 characters")
	private String name;
	@JsonProperty("StudentDept")
	@JsonInclude(Include.NON_NULL)
	@NotBlank(message = "Role should not be empty")
	@Column(name = "student_dep")
	private String dep;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDep() {
		return dep;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	public Student(Long id,
			@NotBlank(message = "Name should not be empty") @Size(min = 6, message = "Name should have atleast 6 characters") String name,
			@NotBlank(message = "Role should not be empty") String dep) {
		super();
		this.id = id;
		this.name = name;
		this.dep = dep;
	}
	
	public Student() {}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dep == null) ? 0 : dep.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (dep == null) {
			if (other.dep != null)
				return false;
		} else if (!dep.equals(other.dep))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", dep=" + dep + "]";
	}
	
}
