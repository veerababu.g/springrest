package com.hcl.mock.exception;

public class StudentNotFoundException extends RuntimeException{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String error;

	public StudentNotFoundException() {
		    super();
		  }
	public StudentNotFoundException(String error){
		super(error);
		this.error = error;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
}
